<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page1 extends CI_Controller {

	public function index()
	{
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('page1/index');
		$this->load->view('template/footer');
	}
}
