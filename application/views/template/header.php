<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Application CHI - GMF</title>

    <link href="<?php echo base_url('template/');?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url('template/');?>font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="<?php echo base_url('template/');?>css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="<?php echo base_url('template/');?>js/plugins/gritter/jquery.gritter.css" rel="stylesheet">


    <link href="<?php echo base_url('template/');?>css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url('template/');?>css/style.css" rel="stylesheet">
     <script type="text/javascript" src="<?php echo base_url('template/');?>js/jquery-2.1.1.js"></script>
    <link rel="icon" type="image/png" href="<?php echo base_url('template/');?>assets/images/icon.png">
    <style type="text/css">
        #footer{
        position:absolute;
        bottom:0;
        width:100%;
        height:60px;   /* tinggi dari footer */
        background:#6cf;
        }
    </style>

</head>
